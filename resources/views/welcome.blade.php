<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Short link</title>
        <link href="//fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
    <div class="wrapper">
        <div class="container">
            <h1>Create short link</h1>
            <div class="form">
                <input type="text" id="link" value="">
                <button id="js-submit">Create</button>
            </div>
            <div id="result"></div>
        </div>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
