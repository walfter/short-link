let button = document.getElementById('js-submit');

button.addEventListener('click', function (event) {
    event.preventDefault();
    let link = document.getElementById('link').value,
        start_caption = button.innerHTML,
        result = document.getElementById('result');
    result.innerHTML = '';
    button.innerHTML = '<span class="loader"></span>';
    console.log(link);
    fetch('/api/links', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            url: link
        })
    }).then(response => response.json())
        .then(response => {
            let answer = document.createElement('div');
            if (response.success) {
                answer = document.createElement('ul');
                let start_url = response.payload.link.start_url,
                    end_url = response.payload.link.end_url,
                    data = '';
                data += `<li><span>Short link:</span><a target="_blank" rel="nofollow noopener noreferrer" href="${start_url}">${start_url}</a></li>`;
                data += `<li><span>Your link:</span><a target="_blank" rel="nofollow noopener noreferrer" href="${end_url}">${end_url}</a></li>`;
                answer.innerHTML = data;
            } else {
                answer.innerHTML = '';
                response.payload.messages.forEach(function (error) {
                    answer.innerHTML += `<div>${error}</div>`;
                })
            }
            result.innerHTML = '';
            result.prepend(answer);
            button.innerHTML = start_caption;
        });
});
