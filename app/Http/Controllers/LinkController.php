<?php

namespace App\Http\Controllers;

use App\Models\Link;

class LinkController extends Controller
{
    public function redirectTo(Link $link)
    {
        return redirect($link->url, 301);
    }
}
