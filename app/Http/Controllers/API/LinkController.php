<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\LinkResource;
use App\Models\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    public function store(Request $request)
    {

        $validated = $request->validate([
            'url' => ['required', 'active_url']
        ]);

        $url = $validated['url'];

        $link = Link::where('url', $url)->first();
        if ($link === null) {
            $link = new Link();
            $link->url = $url;
            $link->makeSlug();
            $link->save();
        }

        return (new LinkResource($link))
            ->response()
            ->setStatusCode($link->wasRecentlyCreated ? 201 : 200);
    }
}
