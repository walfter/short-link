<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LinkResource extends JsonResource
{

    public static $wrap = 'payload';

    public function toArray($request)
    {
        return [
            'success' => true,
            'payload' => [
                'link' => [
                    'slug' => $this->slug,
                    'start_url' => route('link.redirectTo', $this->slug),
                    'end_url' => $this->url
                ]
            ]
        ];
    }
}
