<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Link extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug',
        'url',
    ];

    public function makeSlug(): self
    {
        do {
            $slug = Str::random(config('link.length'));
        } while (Link::where('slug', $slug)->exists());
        $this->slug = $slug;
        return $this;
    }

}
