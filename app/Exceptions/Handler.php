<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        if ($request->is('api/*') || $request->wantsJson() || $request->ajax()) {
            $code = 400;
            if ($e instanceof ModelNotFoundException)
                $code = 404;
            $messages = $e->getMessage();
            if ($e instanceof ValidationException)
                $messages = array_values($e->errors())[0];
            return response()->json([
                'success' => false,
                'payload' => [
                    'messages' => $messages
                ]
            ], $code);
        }
        return parent::render($request, $e);
    }
}
